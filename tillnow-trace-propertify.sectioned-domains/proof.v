Require Import List.

Require Import ltac_utils.

Require Import World.

Require Beque.LTL.ltl.LTL.
Require Beque.newfeat.ltl.LTLUtil.
Require Beque.newfeat.ltl.LTLUtilFacts.

Module pf (w : World).

  Module old := Beque.LTL.ltl.LTL.LTL w.
  Module new := Beque.newfeat.ltl.LTLUtil.LTLUtil w.
  Module newf := Beque.newfeat.ltl.LTLUtilFacts.LTLUtilFacts w.

  Check old.tillnow.
  Check new.tillnow_msg.

  Goal forall oof s e (s_dec : forall m, {s m}+{~s m}) t,
         new.tillnow_msg oof s e t <-> old.tillnow oof s e t.
  Proof.
    induction t.
    {
      compute; split; intro H.
      - destruct H as [ snil [ tnil [ nilpart H ] ] ].
        destruct snil, tnil.
        + exact (proj1 H).
        + discriminate nilpart.
        + discriminate nilpart.
        + discriminate nilpart.
      - contradiction H.
    }
    destruct IHt as [ ifnewthenold ifoldthennew ].
    split; [ intro newpf | intro oldpf ].

    - clear ifoldthennew.
      unfold old.tillnow, new.tillnow_msg in *. unfold new.ltl.tillnow in *.
      destruct newpf as [ sx [ t' [ ppf [ H0 H1 ] ] ] ].
      destruct t'; [ contradiction H0 | ].
      destruct sx.
      + left. clear H1.
        injit ppf. intro. subst m. exact H0.
      + injit ppf. intro. subst m0. subst t.
        simpl. destruct (s_dec a); [ left; assumption | ]. right. split.
        * assert (H1' := H1 nil (a::sx) eq_refl). clear H1. clear ifnewthenold.
          simpl in H1'. unfold new.tillnow_msg_keeps in H1'.
          firstorder.
        * apply ifnewthenold. clear ifnewthenold.
          exists sx. exists (m::t').
          split; [ reflexivity | ].
          split; [ exact H0 | ].
          intros ? ? sxp.
          subst sx.
          assert (H1' := H1 (a::s'') s' eq_refl). clear H1.
          exact H1'.

    - clear ifnewthenold.
      simpl in oldpf.
      destruct (s_dec a) as [ sa | nsa ];
        destruct oldpf as [ sa' | tailpf ];
        [ | | elim nsa; assumption | ].
      + (* starts now *)
        exists nil. exists (a::t).
        split; [ reflexivity | ].
        split; [ exact sa | ].
        intros.
        destruct s''; [ destruct s'; [ | discriminate H ] | discriminate H ].
        simpl. unfold new.tillnow_msg_keeps.
        right. right. assumption.
      + (* starts and tail too *)
        apply newf.tillnow_msg_unfold.
        {
          apply ifoldthennew. exact (proj2 tailpf).
        }
        unfold new.tillnow_msg_keeps.
        right. right. exact sa.
      + (* not starts, but keeps *)
        apply newf.tillnow_msg_unfold.
        {
          apply ifoldthennew. exact (proj2 tailpf).
        }
        unfold new.tillnow_msg_keeps.
        destruct tailpf as [ [ note | oofa ] _ ].
        * left. exact note.
        * right. left. exact oofa.
  Qed.

  Check old.ever_was.
  Check new.ever_was.
  Goal forall p t, old.ever_was p t <-> new.ever_was p t.
  Proof.
    intros; split; intros.
    - destruct H as [ s [ t' H ] ].
      destruct H.
      unfold new.ever_was. unfold new.ltl.tillnow.
      exists s. exists t'.
      split; [ exact H | ].
      split; [ exact H0 | ].
      intros. exact I.
    - unfold old.ever_was. unfold new.ever_was in H. unfold new.ltl.tillnow in H.
      destruct H as [ s [ t' [ ppf [ pt' _ ] ] ] ].
      exists s. exists t'.
      exact (conj ppf pt').
  Qed.

End pf.
