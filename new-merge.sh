#! /bin/bash

set -e
set -u

undo=""
if [ "$1" = -u ]; then
	undo=1
	shift
fi

proj="$1"
branch="$2"
id="${3-}"
if [ -n "$id" ]; then
	id="$id."
fi

projrepo() {
	case "$1" in
		util)
			echo ../beque-core-misc-util/
		;;
		io)
			echo ../beque-core-io/
		;;
		ltl)
			echo ../beque-core-ltl/
		;;
		frame)
			echo ../beque-core-domain/
		;;
		*)
			echo "$1" not a known project >&2
			exit 1
		;;
	esac
}

submoduledir() {
	local id="$1"
	shift
	local branch="$1"
	shift
	local proj="$1"
	shift
	local projbranch="$1"
	echo ".$id$branch--$p--$projbranch"
}

newsubmodule() {
	local branch="$1"
	shift
	local repo="$1"
	shift
	local dir="$1"

	if [ -n "${SUBMODULENAMING:-}" ]; then
		local name="$dir.$RANDOM$RANDOM" # avoid name re-use error
		git submodule add -b "$branch" --name "$name" "$repo" "$dir"
	else
		git submodule add -b "$branch" "$repo" "$dir"
	fi
}

dependencies() {
	for p in util io ltl frame; do
		if [ "$p" = "$1" ]; then
			break
		fi
		echo "$p"
	done
}

for p in `dependencies "$proj"` $proj; do
	sm="`submoduledir "$id" "$branch" "$p" master`"
	if [ -z "$undo" ]; then
		newsubmodule master "`projrepo $p`" "$sm"
	else
		git reset HEAD "$sm"
		rm -rf "$sm"
	fi
done

sm="`submoduledir "$id" "$branch" "$p" "$branch"`"
if [ -z "$undo" ]; then
	newsubmodule $branch "`projrepo $proj`" "$sm"
else
	git reset HEAD "$sm"
	rm -rf "$sm"
fi

pfd="$id$branch"
if [ -z "$undo" ]; then
	mkdir "$pfd"

echo 'BEQUEINSTDIR=../'"$pfd"'/Beque/
COQC=$(COQROOT)coqc

proof.vo: proof.v all
	$(COQC) `cat _CoqProject` $<

all:
	mkdir -p $(BEQUEINSTDIR)' > "$pfd"/Makefile

	for p in `dependencies "$proj"` $proj; do
		sm="`submoduledir "$id" "$branch" "$p" master`"
		echo '	$(MAKE) BEQUEINSTDIR=$(BEQUEINSTDIR) -C ../'"$sm"'/ install' >> "$pfd"/Makefile
	done
	sm="`submoduledir "$id" "$branch" "$p" "$branch"`"
	echo '	$(MAKE) BEQUEINSTDIR=$(BEQUEINSTDIR) BEQUESUBPKG=newfeat -C ../'"$sm"'/ install' >> "$pfd"/Makefile

	echo '(* prove some property of the change here *)' > "$pfd"/proof.v

	echo '-R Beque Beque' > "$pfd"/_CoqProject
else
	rm -rf "$pfd"
fi
